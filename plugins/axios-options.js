export default ({ app, req }) => {
 /*--- Переопределяем фдрес API в зависимости от адреса сайта ---*/
 if (process.server) {
   const URL = require('url').URL;
   const baseURL = new URL(app.$axios.defaults.baseURL);
   const currentHostname = req.headers.host.split(':')[0];

   if (currentHostname !== baseURL.hostname) {
     const port = [80, 443].includes(baseURL.port) ? '' : `:${baseURL.port}`;
     app.$axios.setBaseURL(`${baseURL.protocol}//${currentHostname}${port}${baseURL.pathname}`);
   }
 } else if (process.client) {
   const baseURL = new URL(app.$axios.defaults.baseURL);
   const currentUrl = new URL(document.location);

   if (currentUrl.hostname !== baseURL.hostname) {
     const port = [80, 443].includes(baseURL.port) ? '' : `:${baseURL.port}`;
     app.$axios.setBaseURL(`${baseURL.protocol}//${currentUrl.hostname}${port}${baseURL.pathname}`);
   }
 }
 /*-------------------------------------------------------------*/

 app.$axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';
 app.$axios.defaults.xsrfCookieName = 'csrftoken';
}
