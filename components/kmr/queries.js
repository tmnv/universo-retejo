// Запрос для получения списка всех пользователей
export const uzantoj_query = `
query ($first: Int, $after: String, $serchi: String) {
  lasto: uzantoj(last: 1) {
    pageInfo {
      hasPreviousPage
      startCursor
      endCursor
    }
    edges {
      node {
        uuid
        objId
        id
      }
    }
  }
  uzantoj(first: $first, after: $after, isActive: true, konfirmita: true ,serchi: $serchi, orderBy: ["-krea_dato"]) {
    pageInfo {
      hasNextPage
      startCursor
      endCursor
    }
    edges {
      node {
        id
        objId
        uuid
        unuaNomo {
          enhavo
        }
        duaNomo {
          enhavo
        }
        familinomo {
          enhavo
        }
        sekso
        statistiko {
          miaGekamarado
          miaGekamaradoPeto
          kandidatoGekamarado
          tutaGekamaradoj
          rating
          aktivaDato
        }
        isActive
        konfirmita
        avataro {
          bildoE {
            url
          }
          bildoF {
            url
          }
        }
        statuso {
          enhavo
        }
       kontaktaInformo
      }
    }
  }
}`;

// // Запрос для получения данных конкретного пользователя
export const kmr_query = `
query ($id: Float!) {
  uzantoj(objId: $id) {
    edges {
      node {
        id
        objId
        uuid
        unuaNomo {
          enhavo
        }
        duaNomo {
          enhavo
        }
        familinomo {
          enhavo
        }
        sekso
        statistiko {
          miaGekamarado
          miaGekamaradoPeto
          kandidatoGekamarado
          tutaGekamaradoj
          rating
          aktivaDato
        }
        avataro {
          bildoE {
            url
          }
          bildoF {
            url
          }
        }
        statuso {
          enhavo
        }
        kontaktaInformo
      }
    }
  }
}`;
export const gekamaradoj_query = `
query ($first: Int, $after: String, $serchi: String) {
  lasto: gekamaradoj(last: 1) {
    pageInfo {
      hasPreviousPage
      startCursor
      endCursor
    }
    edges {
      node {
        uuid
        objId
        id
      }
    }
  }
  gekamaradoj(first: $first, after: $after, isActive: true, konfirmita: true ,serchi: $serchi, orderBy: ["-krea_dato"]) {
    pageInfo {
      hasNextPage
      startCursor
      endCursor
    }
    edges {
      node {
        id
        objId
        uuid
        unuaNomo {
          enhavo
        }
        duaNomo {
          enhavo
        }
        familinomo {
          enhavo
        }
        sekso
        statistiko {
          miaGekamarado
          miaGekamaradoPeto
          kandidatoGekamarado
          tutaGekamaradoj
          rating
          aktivaDato
        }
        isActive
        konfirmita
        avataro {
          bildoE {
            url
          }
          bildoF {
            url
          }
        }
        statuso {
          enhavo
        }
       kontaktaInformo
      }
    }
  }
}`;