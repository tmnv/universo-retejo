// https://auth.nuxtjs.org/recipes/extend.html

export default class SiriusoScheme {
  constructor(auth, options) {
    this.$auth = auth;
    this.name = options._name;

    this.options = Object.assign({}, DEFAULTS, options);
  }

  _setToken(token) {
    if (this.options.globalToken) {
      const csrf_token = this.$auth.$storage.getUniversal("csrf-token", false);

      if (csrf_token) {
        this.$auth.ctx.app.$axios.setHeader(
          this.options.csrfTokenName,
          csrf_token
        );
      }

      this.$auth.ctx.app.$axios.setHeader(this.options.tokenName, token);
    }
  }

  _clearToken() {
    if (this.options.globalToken) {
      this.$auth.ctx.app.$axios.setHeader(this.options.csrfTokenName, false);
      this.$auth.ctx.app.$axios.setHeader(this.options.tokenName, false);
    }
  }

  mounted() {
    if (this.options.tokenRequired) {
      const token = this.$auth.syncToken(this.name);
      this._setToken(token);
    }

    return this.$auth.fetchUserOnce();
  }

  async login(data) {
    if (
      typeof data !== "object" ||
      !data.hasOwnProperty("username") ||
      !data.hasOwnProperty("password")
    ) {
      return;
    }

    await this._logoutLocally();

    const ajaxData = {
      query: `
  mutation($login: String!, $password: String!){
    ensaluti(login: $login, password: $password) {
      status
      token
      csrfToken
      konfirmita
    }
  }`,
      variables: { login: data.username, password: data.password }
    };

    const result = await this.$auth.ctx.app.$axios
      .post("registrado/", ajaxData)
      .then(response => {
        if (response.data.data && response.data.data.ensaluti) {
          const resp = response.data.data.ensaluti;

          if (resp.status) {
            this.$auth.$storage.setUniversal("konfirmita", resp.konfirmita);
            this.$auth.$storage.setUniversal(
              "csrf-token",
              resp.csrfToken,
              false
            );
            return resp.token;
          } else {
            this.$auth.$storage.setUniversal("konfirmita", resp.konfirmita);
            return Promise.reject(new Error(resp.message));
          }
        }
      })
      .catch(e => {
        return Promise.reject(e);
      });

    if (this.options.tokenRequired) {
      this.$auth.setToken(this.name, result);
      this._setToken(result);
    }

    // window.location.reload(true);
    return this.fetchUser();
  }

  async fetchUser() {
  

    // alert('asdas')
    // Token is required but not available
    if (this.options.tokenRequired && !this.$auth.getToken(this.name)) {
      return;
    }

    this.$auth.$storage.ctx.store.commit("ui/toggle_loader");

    const ajaxData = {
      query: `
  query{
    mi {
      objId
      uuid
      unuaNomo{
        enhavo
      }
      familinomo{
        enhavo
      }
      avataro{
        bildoE {
          url
        }
        bildoF {
          url
        }
      }
    }
  }`
    };

    // Try to fetch user and then set
    const user = await this.$auth.ctx.app.$axios
      .post("registrado/", ajaxData)
      .then(response => {
        if (response.data.data && response.data.data.mi) {
          setTimeout(() => {
            this.$auth.$storage.ctx.store.commit("ui/toggle_loader");
          }, 3500);
          return Promise.resolve(response.data.data.mi);
        } else {
          setTimeout(() => {
            this.$auth.$storage.ctx.store.commit("ui/toggle_loader");
          }, 3500);
          return this.$auth.callOnError(new Error("Authentication required!"));
        }
      })
      .catch(e => {
        setTimeout(() => {
            this.$auth.$storage.ctx.store.commit("ui/toggle_loader");
          }, 3500);
        return this.$auth.callOnError(e);
      });

    this.$auth.setUser(user);
  }

  async logout() {
    // Если нет активного ключа
    if (!this.$auth.getToken(this.name)) {
      return this._logoutLocally();
    }

    const ajaxData = {
      query: `
  mutation {
    elsaluti {
      status
      message
    }
  }`
    };

    const result = await this.$auth.ctx.app.$axios
      .post("registrado/", ajaxData)
      .then(response => {
        const result = this._logoutLocally();
        // window.location.reload(true);
        this.$auth.$storage.setUniversal("konfirmita", undefined);
        return result;
      })
      .catch(e => {
        this.$auth.callOnError(e);
        return Promise.reject(e);
      });
  }

  async _logoutLocally() {
    if (this.options.tokenRequired) {
      this._clearToken();
    }

    return this.$auth.reset();
  }
}

const DEFAULTS = {
  tokenRequired: true,
  globalToken: true,
  tokenName: "X-Auth-Token",
  csrfTokenName: "X-CSRFToken"
};
